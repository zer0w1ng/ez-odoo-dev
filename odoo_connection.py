#!/usr/bin/env python

from __future__ import print_function
import requests
import odoorpc

class OdooConnection(object):

    def __init__(self, host, port, admin_pwd, super_password, admin="admin", lang="en_US"):
        self.host = host
        self.port = int(port)
        self.admin_password = admin_pwd
        self.super_password = super_password
        self.language = lang
        self.admin = admin


    def get_session(self, db, user=None, pwd=None, login=True, timeout=None):

        if self.port==443:
            odoo = odoorpc.ODOO(self.host, port=self.port, protocol='jsonrpc+ssl')
        else:
            odoo = odoorpc.ODOO(self.host, port=self.port)

        odoo.config['timeout'] = timeout
        if login:
            if not user:
                user = self.admin
            if not pwd:
                pwd = self.admin_password
            odoo.login(db, user, pwd)
        return odoo


    def backup_database(self, db_name, filename, timeout=1200):
        print("Backup database %s to %s..." % (db_name, filename))
        data = {
            'master_pwd': self.super_password,
            'name': db_name,
        }
        proto = (self.port in [443,'443'] and 'https') or 'http'
        url = "%s://%s/web/database/backup" % (proto, self.host)
        print("URL:", url)
        r = requests.post(url, data = data)
        i = 0
        with open(filename, 'wb') as fd:
            for chunk in r.iter_content(chunk_size=128):
                fd.write(chunk)
                i += 1
                if i%8192==0:
                    print("    downloaded", (i*128)/1024/1024, "MB")
        print("    Backup done.")


    def duplicate_database(self, db_name, new_db_name):
        odoo = self.get_session(db_name, login=False)

        if db_name not in odoo.db.list():
            print("Duplicate failed! Source database", db_name, "does not exists.")
            return

        if new_db_name in odoo.db.list():
            print("Duplicate failed! Target database", db_name, "already exists.")
            return

        print("Duplicating database", db_name, "...")
        odoo.db.duplicate(self.super_password, db_name, new_db_name)
        print("    Database", db_name, "duplicated to", new_db_name, ".")
        print()


    def restore_database(self, db_name, filename, timeout=1200):
        odoo = self.get_session(db_name, login=False)
        if db_name in odoo.db.list():
            print("Restore failed! Source database", db_name, "exists.")
            return

        print("Restoring database", db_name, "from", filename, "...")
        dump_file = open(filename)
        timeout_backup = odoo.config['timeout']
        odoo.config['timeout'] = timeout
        odoo.db.restore(self.super_password, db_name, dump_file) # doctest: +SKIP
        odoo.config['timeout'] = timeout_backup
        print("Restoring database", db_name, "done.")
        print()


    def create_database(self, db_name, demo=False):
        odoo = self.get_session(db_name, login=False)
        if db_name not in odoo.db.list():
            print("Creating database", db_name, "...")
            odoo.db.create(self.super_password, db_name, demo=demo, lang=self.language, admin_password=self.admin_password)
            print("Database", db_name, "created.")
            print()
            return True
        else:
            print("Create failed! Database", db_name, "already exists.")
            print()
            return False


    def drop_database(self, db_name):
        odoo = self.get_session(db_name, login=False)
        if db_name in odoo.db.list():
            odoo.db.drop(self.super_password, db_name)
            print("Database", db_name, "dropped.")
        else:
            print("Database", db_name, "do not exist.")
        print()



    def uninstall_modules(self, db_name, module_names):
        print("Uninstall modules on %s: %s" % (db_name, module_names))
        odoo = self.get_session(db_name)
        Module = odoo.env['ir.module.module']
        for module_name in module_names:
            module_ids = Module.search([
                ('name', '=', module_name),
                ('state', 'in', ['installed', 'to upgrade', 'to remove', 'to install'])
            ])
            print("Uninstalling module", module_name, "...")
            if module_ids:
                Module.button_immediate_uninstall(module_ids)
                print("    Module", module_name, "uninstalled.")
            else:
                print("    Module", module_name, "was not installed")
        print()


    def install_modules(self, db_name, modules):
        print("Install modules on %s: %s" % (db_name, modules))
        odoo = self.get_session(db_name)
        Module = odoo.env['ir.module.module']
        for module_name in modules:
            module_ids = Module.search([
                ('name', '=', module_name),
                ('state', 'not in', ['installed', 'to upgrade']),
            ])
            print("Installing module", module_name, "...", module_ids)
            if module_ids:
                Module.button_immediate_install(module_ids)
                print("    Module", module_name, "installed.")
            else:
                print("    Module", module_name, "was not installed. Might already be installed.")
        print()


    def upgrade_modules(self, db_name, modules):
        print("Upgrade modules on %s: %s" % (db_name, modules))
        odoo = self.get_session(db_name)
        Module = odoo.env['ir.module.module']
        for module_name in modules:
            module_ids = Module.search([
                ('name', '=', module_name),
                ('state', 'in', ['installed', 'to upgrade']),
            ])
            if module_ids:
                print("Upgrading module", module_name, "...", module_ids)
                Module.button_immediate_upgrade(module_ids)
                print("    Module", module_name, "upgraded.")
            else:
                print( "    Module", module_name, "was not upgraded.")
        print()


    def update_module_list(self, db_name):
        print("Update module list on %s." % (db_name))
        odoo = self.get_session(db_name)
        updated, new = odoo.env['ir.module.module'].update_list()
        print('    %s: %s modules updated, %s modules new' % (db_name, updated, new))
        print()


if __name__ == "__main__":

    #test
    conn = OdooConnection('localhost','8069','pass1234','passw0rd')
    conn.create_database("dbtest0")
    conn.duplicate_database("dbtest0", "dbtest1")
    conn.uninstall_modules("dbtest1", [
        'im_chat',
        'im_livechat',
        'im_odoo_support'
    ])
    conn.drop_database("dbtest0")
    conn.drop_database("dbtest1")


#
