#!/usr/bin/env python

from __future__ import print_function
import glob
import sys
import csv
import multiprocessing

from odoo_connection import *
from batch_thread import *

def read_csv(filename):
    data = []
    with open(filename) as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            data.append(row)

    print("CSV %s: %s recods" % (filename, len(data)))

    if data:
        return data[0], data[1:]
    else:
        return [], []

def upload_to_odoo(conn, table, filename=False, path=".", chunk_size=100, threads=False):
    if threads:
        THREADS = threads
    else:
        THREADS = multiprocessing.cpu_count()

    if filename:
        header, data = read_csv("%s/%s.csv" % (path, filename))
    else:
        header, data = read_csv("%s/%s.csv" % (path, table))
    print("Multi-load to %s table." % table, THREADS)

    sz = len(data)/chunk_size
    if sz<THREADS:
        chunk_size = int(1 + (len(data)/THREADS))
    if chunk_size<10: 
        chunk_size = 20

    multi_load(conn, table, header, data, threads=THREADS, chunk_size=chunk_size)
    print()

#################################################

if __name__=="__main__":

    THREADS =  multiprocessing.cpu_count()

    conn_params = dict(
        host = "localhost",
        port = 80,
        admin_pwd = "ebfeYUx2i5D2PZ6",
        dbname = "ecb_test",
        #dbname = "test-bill1",
        super_password = "pass1234",
    )

    if 0:
        # network
        conn_params.update({
            'host': "ez1.eztechsoft.com",
            'port': 443,
            'super_password': "ebfeYUx2i5D2PZ6",
        })

    if 1:
        upload_to_odoo(conn_params, "res.partner")
        upload_to_odoo(conn_params, "product.pricelist")
        upload_to_odoo(conn_params, "product.product")
        upload_to_odoo(conn_params, "product.pricelist.item")

        upload_to_odoo(conn_params, "sale.order")
        upload_to_odoo(conn_params, "sale.order.line")

    if 0:
        upload_to_odoo(conn_params, "account.account")


