import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ez-odoo-dev", 
    version="0.0.1",
    author="Regulus Berdin",
    author_email="rberdin@eztechsoft.com",
    description="EzTech Odoo dev package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/zer0w1ng/ez-odoo-dev.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)