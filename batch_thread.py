import threading
from time import sleep
import logging
import sys
from odoo_connection import OdooConnection
import multiprocessing

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)
out = logging.StreamHandler(sys.stdout)
_logger.addHandler(out)

CHUNK_SIZE = 100
THREADS = 4

def chunks(data, chunksize):
    for i in range(0, len(data), chunksize):
        yield data[i:i+chunksize]

class BatchThread(object):

    def __init__(self, max_connection):
        self.semaphore = threading.BoundedSemaphore(max_connection)
        self.max_thread_semaphore = threading.BoundedSemaphore(max_connection * 4)
        self.thread_list = []

    def spawn_thread(self, fun, args, kwarg=None):
        def wrapper(args, kwarg):
            kwarg = kwarg or {}
            self.semaphore.acquire()
            try:
                fun(*args, **kwarg)
            except:
                self.semaphore.release()
                self.max_thread_semaphore.release()
                raise
            self.semaphore.release()
            self.max_thread_semaphore.release()

        self.max_thread_semaphore.acquire()
        thread = threading.Thread(None, wrapper, None, [args, kwarg], {})
        thread.start()
        self.thread_list.append(thread)

    def wait(self):
        for t in self.thread_list:
            t.join()

    def thread_number(self):
        return len(self.thread_list)


#######################################

def login(conn):
    zconn = OdooConnection(
        conn['host'],
        conn['port'],
        conn['admin_pwd'],
        conn['super_password'],
    )
    return zconn.get_session(conn['dbname'])

def thread_load(TableObj, name, header, data):
    _logger.info("%s: load records=%d" % (name, len(data)))
    res = TableObj.load(header, data)
    _logger.info("%s: res=%s" % (name, res))

def multi_load(conn, table, header, data, threads=0, chunk_size=100):
    odoo = login(conn)

    if not threads:
        threads = multiprocessing.cpu_count()

    bt = BatchThread(threads)
    odoo.env.context.update({
        'tracking_disable' : True,
    })
    _logger.info("Loading data to table: %s recs=%d" % (table, len(data)))
    TableObj = odoo.env[table]
    i = 0
    for chunk in chunks(data, chunk_size):
        params = [TableObj, "Thread %02d" % i, header, chunk]
        bt.spawn_thread(thread_load, params)
        i += 1
    bt.wait()
    odoo.logout()

#######################################

def delete_records(odoo, table, name, ids):
    print("**",name, len(ids))
    res = odoo.execute(table, "unlink", ids)
    print('  ',name,"res =", res)

def multi_delete(conn_param, table, filter=[], threads=0, chunk_size=1000):
    print("Multidelete records: filter", filter)
    odoo = login(conn_param)
    odoo.env.context.update({
        'tracking_disable' : True,
    })
    ids = odoo.env[table].search(filter)
    print("No of records:", len(ids))

    if not threads:
        threads = multiprocessing.cpu_count()

    bt = BatchThread(threads)
    i = 0
    for chunk in chunks(ids, chunk_size):
        params = [odoo, table, "Thread %02d" % i, chunk]
        bt.spawn_thread(delete_records, params)
        i += 1
    bt.wait()
    odoo.logout()


#######################################

def testx(name, n):
    for i in range(n):
        _logger.info("Thread %s: %d" % (name, i))
        sleep(0.1)


if __name__=="__main__":
    _logger.info("Start...")
    bt = BatchThread(4)

    for i in range(16):
        bt.spawn_thread(testx, ["test_%02d" % i, 10])

    bt.wait()



#
