import odoorpc
import os
import time
import import_csv

import configparser
import sys
import getopt

def login(conn, timeout=None):
    db = conn["dbname"]
    pwd = conn["admin_pwd"]
    host = conn["host"]
    port = conn["port"]
    if port==443:
        odoo = odoorpc.ODOO(host, port=port, protocol='jsonrpc+ssl')
    else:
        odoo = odoorpc.ODOO(host, port=port)

    odoo.config['timeout'] = timeout

    #print(db, pwd)
    odoo.login(db, 'admin', pwd)
    return odoo


def restore(conn, filename, drop=False):
    if not drop:
        db = conn["dbname"]
        pwd = conn["admin_pwd"]
        spwd = conn["super_password"]
        host = conn["host"]
        port = conn["port"]
        if port==443:
            odoo = odoorpc.ODOO(host, port=port, protocol='jsonrpc+ssl')
        else:
            odoo = odoorpc.ODOO(host, port=port)

        res = odoo.db.drop(spwd, db)
        print("Drop (%s): %d" % (db, res))
        odoo.logout()
        print()
        
    print("Restoring [%s]: %s..." % (db, filename))
    http = "http"
    if conn["port"]==443:
        http = "https"
    cmd = "curl -F 'master_pwd=%s' -F backup_file=@%s -F 'copy=true' -F 'name=%s' %s" % (
        spwd, filename, db, "%s://%s/web/database/restore" % (http, host) 
    )
    print(cmd)
    os.system(cmd)
    print("    done restore.")


def update_modules_list(conn, timeout=None):
    odoo = login(conn, timeout=timeout)
    db = conn["dbname"]

    print("Update module list (%s)" % (db))
    updated, new = odoo.env['ir.module.module'].update_list()
    print('    %s modules updated, %s modules new' % (updated, new))
    print()
    return odoo


def install(conn, modules, update_modules=True):
    db = conn["dbname"]
    timeout = 3000
    if update_modules:
        odoo = update_modules_list(conn, timeout=timeout)
    else:
        odoo = login(conn, timeout=timeout)

    print("Install modules (%s): %s" % (db, modules))
    Module = odoo.env['ir.module.module']
    for module_name in modules:
        module_ids = Module.search([
            ('name', '=', module_name),
            ('state', 'not in', ['installed', 'to upgrade']),
        ])
        print("    Installing [%s]: id=%s" % (module_name, module_ids))
        if module_ids:
            Module.button_immediate_install(module_ids)
            print("        Module", module_name, "installed.")
        else:
            print("        Module", module_name, "was not installed. Might already be installed.")
    print()
    return odoo


def update(conn, modules, update_modules=True):
    #db = conn["dbname"]
    if update_modules:
        odoo = update_modules_list(conn)
    else:
        odoo = login(conn)

    Module = odoo.env['ir.module.module']
    for module_name in modules:
        module_ids = Module.search([
            ('name', '=', module_name),
            ('state', 'in', ['installed', 'to upgrade']),
        ])
        if module_ids:
            print("Upgrading module", module_name, "...", module_ids)
            Module.button_immediate_upgrade(module_ids)
            print("    Module", module_name, "upgraded.")
        else:
            print("    Module", module_name, "was not upgraded.")
    print()

    return odoo


def logout(odoo):
    odoo.logout()


def create(conn, db_name, demo=False, lang="en_US", drop=True):
    db = conn["dbname"]
    pwd = conn["admin_pwd"]
    spwd = conn["super_password"]
    host = conn["host"]
    port = conn["port"]
    if port==443:
        odoo = odoorpc.ODOO(host, port=port, protocol='jsonrpc+ssl')
    else:
        odoo = odoorpc.ODOO(host, port=port)

    if drop:
        res = odoo.db.drop(spwd, db)
        print("Drop (%s): %d" % (db, res))
        print()

    #odoo = update_modules_list(conn)
    print("Creating database", db_name, "...")
    odoo.db.create(
        spwd, 
        db, 
        demo=demo, 
        lang=conn.get('language', lang), 
        admin_password=pwd
    )
    print("Database", db_name, "created.")
    print()


###########################################################

def start(config_file):
    config = configparser.ConfigParser()
    config.read(config_file)
    #config = get_config('config.txt')

    connection_string = config['setup']['connection']
    connection = config[connection_string]

    #process_string = config['setup']['process']
    #process1 = config[process_string]

    conn_params = {
        'host': connection.get('host'),
        'port': int(connection.get('port','80')), 
        'super_password': connection.get('super_password'),
        'admin_pwd': connection.get('admin_pwd'),
        'host': connection.get('host'),
        'dbname': connection.get('dbname'),
    }

    print("CONNECTION:", conn_params)
    print()

    process_string = config['setup']['process']
    local_site = connection_string=='local'
    
    for process in process_string.split(","):
        print("-"*40)
        print(process.upper().replace('_',' '))

        #print(config[process]['process'])
        if config[process]['process']=='create_db': 
        	#def create(conn, db_name, demo=False, lang="en_US", drop=True):
            db_name = config[process]['db']
            demo = 	config[process].get('demo')
            drop = 	config[process].get('drop')
            create(conn_params, db_name, demo=demo, drop=drop)
            print("    done.")
            print()

        if config[process]['process']=='run_command': 
            cmd = config[process]['command']
            print(cmd)
            os.system(cmd)
            time.sleep(float(config[process].get('sleep',0)))
            print("    done.")
            print()
            
        if config[process]['process']=='restore_backup': 
            restore(conn_params, config['restore_backup']['restore_file'])
            cmd = config[process]['command']
            print(cmd)
            os.system(cmd)
            time.sleep(float(config[process].get('sleep',0)))
            print("    done.")
            print()

        if config[process]['process']=='install_modules': 
            #print("INSTALL MODULES...")
            update_modules = int(config[process].get('update','0'))
            modules = config[process]['modules'].split(',')
            m2 = [m.strip() for m in modules]
            odoo = install(conn_params, [m2], update_modules=update_modules)
            odoo.logout()

        if config[process]['process']=='update_modules': 
            update_modules = int(config[process].get('update','0'))
            modules = config[process]['modules'].split(',')
            m2 = [m.strip() for m in modules]
            odoo = update(conn_params, [m2], update_modules=update_modules)
            odoo.logout()

        if config[process]['process']=='install_data': 
            #print("INSTALL DATA...")
            cfg = config[process]
            for data in cfg:
                if data[:4]=='data':
                    s = cfg[data].split(",")
                    if len(s)>=3:
                        threads = int(s[2])
                    else:
                        threads = False
                    print(s)
                    if len(s)==1:
                        import_csv.upload_to_odoo(conn_params, s[0])
                    else:
                        import_csv.upload_to_odoo(conn_params, s[0], path=s[1], threads=threads)

###########################################################

def print_help(argv):
    print('Usage:')
    print('  %s -h                      [help]' % argv[0])
    print('  %s -c                      [config file]' % argv[0])

###########################################################

def parse_config():
    #start("config.txt")
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hc:",["help=","config="])
    except getopt.GetoptError:
        print_help(sys.argv)
        sys.exit(2)

    if not opts:
        print_help(sys.argv)
        sys.exit()

    config_file = False
    for opt, arg in opts:
        if opt == '-h':
            print_help(sys.argv)
            sys.exit()
        elif opt in ("-c") and arg:
            config_file = arg
            #print("Cf")

    if config_file:
        #print(config_file)
        start(config_file)

if __name__=="__main__":
    start("config.txt")
