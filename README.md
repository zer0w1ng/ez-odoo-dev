# Odoo Module Development Installer

Setup Odoo development environment using a config file.

Run setup:
```
python3 -m pip install --user --upgrade setuptools wheel
python3 setup.py sdist bdist_wheel
```

Install program as binary:
```
python3 -m pip install pyinstaller
pyinstaller --onefile ez-odoo-dev
```

Add in PIP database:
```
python3 -m pip install --user --upgrade twine
python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
```

Install program in PIP:
```
python3 -m pip install --index-url https://test.pypi.org/simple/ --no-deps ez-odoo-dev
```

